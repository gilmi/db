{-

We'll start by looking at a general structure of a haskell file (module). A module contains a few things:

* Declaration of the module (should be the same as the filepath) - should be omitted or Main for the entry point module
* List of declarations to export
* Value and function declarations

Note that Haskell does not allow naked expressions or statements at the top-level (like python or javascript)

-}


-- Declaration of the module name
module DB.Simple
-- List of declarations to export
where

-- import other modules
import DB.Utils

-- Module body - declarations


-- 1 -- hello world, check that everything is working

run = putStrLn "Hello, World!"

-- 2 -- define a table structure and an example, use show to print it

type Value = Integer

type Column = (String, Value)

type Row = [Column]

type Table = [Row]

t1 :: Table
t1 =
  [ [ ("x", 1), ("y", 2) ]
  , [ ("x", 3), ("y", 4) ]
  , [ ("x", 4), ("y", 5) ]
  , [ ("x", 6), ("y", 7) ]
  ]

-- 3 -- validate table structure

ppTableData table =
  if checkTable table
     then ppTable table
     else "Inconsistent data"

-- 4 -- define database and an example

type Database = [(String, Table)]

db1 =
  [ ("t1", t1)
  , ("t2", t1)
  , ("t3", t3)
  ]

t3 =
  [ [ ("x", 1) ]
  ]

mymap func list =
  if null list
    then []
    else func (head list) : mymap func (tail list)

ppDBNames db =
  intercalate ", " (mymap getName db)

-- 5 -- interact with user input, print the table the user names

prompt' = do
  putStrLn (ppDBNames db1)
  input <- getLine
  maybe
    (putStrLn ("Table named " ++ trim input ++ " not found."))
    (compose putStrLn ppTableData)
    (lookup (trim input) db1)

prompt'' = do
  putStrLn (ppDBNames db1)
  input <- getLine
  let mTable = lookup (trim input) db1
  if isJust mTable
    then
      putStrLn (ppTableData (fromJust mTable))
    else
      putStrLn ("Table named " ++ trim input ++ " not found.")

-- 6

prompt = do
  putStrLn (ppDBNames db1)
  input <- getLine
  case lookup (trim input) db1 of
    Just table ->
      putStrLn (ppTableData table)
    Nothing ->
      putStrLn ("Table named " ++ trim input ++ " not found.")

-- 6 -- same as 6 but add a loop

looper = do
  putStrLn (ppDBNames db1)
  input <- getLine
  case trim input of
    ":quit" -> pure ()
    tname -> do
      case lookup tname db1 of
        Just table ->
          putStrLn (ppTableData table)
        Nothing ->
          putStrLn ("Table named " ++ trim input ++ " not found.")
      looper


-- 7 -- Define a Query structure without restricts and write an example

sampleQuery = Select [("x", "x1"), ("x", "x2")] (Table "t1")

-- The structure of a query
data Query
  = Values Table
  | Table String
  | Select [(String, String)] Query

-- 8 -- Optional: Add a Restrict structure and an example

  | Restrict Cond Query
  deriving (Show, Read)

-- The structure of a condition
data Cond
  = Equals Arg Arg
  | Not Cond
  | And Cond Cond
  deriving (Show, Read)

-- An argument to a condition
data Arg
  = ArgLit Value
  | ArgCol String
  deriving (Show, Read)



sampleRestrictedQuery =
  Restrict
    (Equals (ArgLit 1) (ArgCol "x2"))
    sampleQuery

-- 9 -- Interpret query

interpret :: Database -> Query -> Table
interpret db query = case query of
  Values table ->
    table

  Table tableName ->
    case lookup tableName db of
      Nothing -> error ("Could not find table " ++ tableName ++ " in db.")
      Just t -> t

  Select selectList innerQuery ->
    let
      innerQueryResult =
        interpret db innerQuery

      selectRow row =
        map (\(col, newName) -> (newName, lookupCol col row)) selectList
    in
      map selectRow innerQueryResult

-- 10 -- Optional: Add Restrict interpretation

  Restrict cond innerQuery ->
    let
      innerQueryResult = interpret db innerQuery
    in
      filter (interpretCond cond) innerQueryResult

interpretCond cond row = case cond of
  Equals arg1 arg2 ->
    interpretArg row arg1 == interpretArg row arg2

  Not cond ->
    not (interpretCond cond row)

  And cond1 cond2 ->
    interpretCond cond1 row && interpretCond cond2 row

interpretArg row arg = case arg of
  ArgLit v -> v
  ArgCol name -> lookupCol name row


testSample1 = putStrLn (ppTableData (interpret db1 sampleRestrictedQuery))

-- 11 -- Optional -- Add a network server for querying the data

handleSocketMsgs processMsg soc = do
  msg <- recv soc 4096
  result <- catch
    (pure (processMsg msg))
    (\(SomeException e) -> pure (show e))
  _ <- send soc (result ++ "\n")
  pure ()


handleQuery msg = case reads msg of
  [(query,_)] -> ppTableData (interpret db1 query)
  x -> "Could not read query. Got: " ++ show x

runServer = runServerLoop 1337 (handleSocketMsgs handleQuery)
